#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use CGI::Cookie;
use CGI::Session;


$username = param('username') || '';
$password = param('password') || '';
$username = substr $username, 0, 256;


$size = "medium";
$users_dir = "dataset-$size/users";
$cur_user_dir = "$users_dir/$username";

if ($username && $password) {
    
    #if the directory exists print this..
    if (-d $cur_user_dir) {
    
    
         # compare with actual pw
        $actual_password = get_password($cur_user_dir);
        $actual_password =~ s/\s+//;
        
        # if password is true, cont. else print invalid form page and re-enter data..
        if ($actual_password eq $password) {
            $session = CGI::Session->new();
            $session->expire(3600);
            $CGISESSID = $session->id();
            $sid_cookie = cookie('sid', $CGISESSID);
            $user_cookie = cookie('username', $username);
            print "Set-Cookie: $user_cookie\n";
            print "Set-Cookie: $sid\n";
            print $session->header(-location =>"bitter.cgi");

            
            
        } else {
            print page_header(), invalid_details_form();
        }
    } else {
        print page_header(), invalid_details_form();
    }
} elsif(param('action') eq 'logout') {
    $session = CGI::Session->load() or die CGI::Session->errstr;
    $session->delete();
    print $session->header(-location=>'login.cgi');
    
} else {
    print page_header(), standard_form();
}

print end_html;
exit(0);

sub get_password {
    my $user_to_show = $_[0];
    my $details_filename = "$user_to_show/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $! ";
    my $details = join "<br>", <$p>;
    close $p;

    my @details = split /<br>/, $details;    
    
    foreach my $val (@details) {
        if ($val =~ /password:/) {
            $val =~ s/password: //;
            return $val;
        }
    }
}

sub standard_form {
    return <<eof
        <div class="container">
            <form class="form-signin" method="post">
                <h2 class="form-signin-heading">Welcome to Bitter!<br>Please sign in</h2>
                <label for="inputUsername" class="sr-only">Username</label>
                <input type="text" id="inputUsername" class="form-control" placeholder="Username" name='username' required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name='password' required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label> 
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div> <!-- /container -->    
eof
    ;
}

sub invalid_details_form {
    return <<eof
        <div class="container">
            <form class="form-signin" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>
                <p>Invalid Username or Password!<br>Please try again.</p>
                <label for="inputUsername" class="sr-only">Username</label>
                <input type="username" id="inputUsername" class="form-control" placeholder="Username" name='username' required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name='password' required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label> 
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div> <!-- /container -->    
eof
    ;
}

sub page_header {
    return header,
        start_html(-title => 'Bitter Login', -style => [{src=>"css/bootstrap.min.css"},
                                                  {src=>"login.css"},
                                                  {src=>"css/bootstrap-theme.css"},
                                                  {src=>"css/boostrap.css"},
                                                  {src=>"css/bootstrap-theme.min.css"},],);
}
