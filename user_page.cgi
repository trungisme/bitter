#!/usr/bin/perl -w

#Tu-Trung Richie Lam 20/10/15

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;
use POSIX qw( strftime );
use CGI::Cookie;


sub initial() {

     #get cookies ~~~~~
    %cookies = fetch CGI::Cookie;
    my $is_auth = 0; # set false i.e. not yet authorised
    $is_auth = $cookies{'is_auth'}->value if $cookies{'is_auth'};
    
    print page_header, "\n";
    warningsToBrowser(1);
    
	# define some global variables
	$dataset_size = "small"; 
	$users_dir = "dataset-$dataset_size/users";
	$bleats_dir = "dataset-$dataset_size/bleats";
    
    print_user_page();
}

#
# specific function to run user page..
#
sub print_user_page {
    my $user = cookies{'username'};
    my $dir = "$users_dir/$user";
    print user_body();
    print user_details($dir);
    print user_bleats($dir);
    print page_trailer();

}


#
# Show fancy bar at top
#
sub user_body {
    my $body = <<eof
<div class="container">
    <div class="header clearfix">
        <nav>
        <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="#">Home</a></li>
            <li role="presentation"><a href="#">About</a></li>
            <li role="presentation"><a href="#">Contact</a></li>
        </ul>
        </nav>
        <h3 class="text-muted">Bitter</h3>
    </div>
eof
    ;
    return $body;
}
  
sub user_bleats {

    my $user_to_show  = $_[0];
    my $bleats_list = "$user_to_show/bleats.txt";
    
    #bleats details
    my %in_reply_to = ();  # bleat , given by key is a response to value.
    my %bleat_content = (); # contains content of bleat
    my %unix_time = ();
    my %longitude = ();
    my %latitude = ();
    
    
    
    open my $p, "$bleats_list" or die "can not open $bleats_list: $!";
    my @bleats = split / |\t|\n/, lc(do {local $/; <$p> }); 
    close $p;
    
    # this print checks if corresponding bleats file has been opened and read.
    #print div({-class=>"col-md-2"}, "@bleats\n"), "\n";
    
    
    
    for $bleats (@bleats) {
        open my $p, "$bleats_dir/$bleats" or die "can not open $bleats_dir/$bleats: $!";
        my $bleat_details = join "<br>", <$p>;
        close $p;
        my @details = split /<br>/, $bleat_details;
        
        foreach my $val (@details) {
            if ($val =~ /username:/) {
                $val =~ s/username: //;
                
                # makes sure that the username correspondes to the users page..
                if ($val ne $username{$user_to_show}) { 
                    print "you fucked up somewhere \n";
                }
            
            # bleat , given by key is a response to value.    
            } elsif ($val =~ /in_reply_to:/) {
                $val =~ s/in_reply_to: //;
                $in_reply_to{$bleats} = $val;
                
            # store content of bleats
            } elsif ($val =~ /bleat:/) {
                $val =~ s/bleat: //;
                $bleat_content{$bleats} = $val;    
            
            # time of bleat    
            } elsif ($val =~ /time:/) {
                $val =~ s/time: //;
                $unix_time{$bleats} = $val;
            
            # store location
            } elsif ($val =~ /longitude:/) {
                $val =~ s/longitude: //;
                $longitude{$bleats} = $val;
            } elsif ($val =~ /latitude:/) {
                $val =~ s/latitude: //;
                $latitude{$bleats} = $val;
            }
        }

        
    }   
    
    # order the bleats in reverse chronological order
    my @ordered_bleats = ();
    foreach my $bleat_no (sort { $unix_time{$b} <=> $unix_time{$a} } keys %unix_time) {
        push @ordered_bleats, $bleat_no;
    }
    
    my $body = "<div class =\"col-md-6\">\n";
    
    foreach my $bleat (@ordered_bleats) {
        # convert to readable date format.
        my $dt = strftime ("%d-%m-%Y %H:%M:%S", localtime ($unix_time{$bleat}));
        $body .= <<eof
        
<div class="row">
    <blockquote class="twitter_tweet">
        <h3><b>$full_name{$user_to_show}</b></h3>
        <h4><i>\@$username{$user_to_show}</i></h4>
        <h5>$dt</h5>
        <--!<h5>Longitude: $longitude{$bleat} Latitude: $latitude{$bleat}</h5>!-->
        <h5>$bleat</h5>
        <p>$bleat_content{$bleat}</p>
    </blockquote>
</div>
eof
            ;
    }
    
    
    
    #$body .= "@ordered_bleats", "\n"; 
    
    $body .= <<eof
    </div>
</div>
    <footer class="footer">
        <p>&copy; BitterSweet 2015</p>
    </footer>
</div> <!-- /container -->
eof
    ;
    
    return $body;
}


#get user details from database
sub get_user_info {
    
    my $user_to_show = $_[0];
    my $details_filename = "$user_to_show/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $! ";
    my $details = join "<br>", <$p>;
    close $p;
    
    my @details = split /<br>/, $details;    
    
    # filter each detail and store it in the hash
    # as of now, unsure if security breach.. keep in mind for later.
    foreach my $val (@details) {
    
    
        if ($val =~ /password:/) {
            $val =~ s/password: //;
            $password{$user_to_show} = $val;
        } elsif ($val =~ /email:/) {
            $val =~ s/email: //;
            $email{$user_to_show} = $val;
        } elsif ($val =~ /full_name:/) {
            $val =~ s/full_name: //;
            $full_name{$user_to_show} = $val;        
        } elsif ($val =~ /username:/) {
            $val =~ s/username: //;
            $username{$user_to_show} = $val;
        } elsif ($val =~ /home_suburb:/) {
            $val =~ s/home_suburb: //;
            $home_suburb{$user_to_show} = $val;
        } elsif ($val =~ /home_longitude:/) {
            $val =~ s/home_longitude: //;
            $home_longitude{$user_to_show} = $val;
        } elsif ($val =~ /home_latitude:/) {
            $val =~ s/home_latitude: //;
            $home_latitude{$user_to_show} = $val;
        }
    }
}

#
# show user details
#
sub user_details {

    #get user information
    my $user_to_show = $_[0];
    get_user_info($user_to_show);
    
    #get profile picture for the user
    my $profile_pic = "$user_to_show/profile.jpg";
    my $img = img({src=>"http://s3.amazonaws.com/FringeBucket/bitter_200.jpg", alt=>'Default Picture', class=>"img-circle"});
    $img = img({src=>$profile_pic, alt=>'Profile Picture', class=>"img-circle"}) if -f $profile_pic;

    #empty string just in case
    my $details =<<eof
    
<div class="row"> 
    <div class="col-md-3">$img
        <h2>$full_name{$user_to_show}</h2>
        <h4><b>About Me</b></h4>
        <ol class="list-unstyled"> 
            <li>\@$username{$user_to_show}</li>
            <li>Suburb: $home_suburb{$user_to_show}</li>
            <li>Longitude: $home_longitude{$user_to_show}</li>
            <li>Latitude: $home_latitude{$user_to_show}</li>
        </ol>
        
    </div>
eof
    ;

    return "\n$details\n",
        '<p>'.
        start_form, "\n",
        hidden('n'), "\n",
        submit({-class => "bitter_button", -value => 'Next user'}), "\n",
        end_form, "\n<br>";
}   

#
# HTML placed at the top of every page
#
sub page_header {
    return header,
        start_html(-title => "Bitter", -style => [{src=>"css/bootstrap.min.css"},
                                                  {src=>"bitter.css"},],);
}

#
# HTML placed at the bottom of every page
# It includes all supplied parameter values as a HTML comment
# if global variable $debug is set
#
sub page_trailer {
    return end_html;
}

main();
