#!/usr/bin/perl -w
use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use CGI::Cookie;
use CGI::Session;
use POSIX qw( strftime );

#globals..
$regex = lc param('q') || '';
$size = "medium";
%bleat_content = ();
%username = ();
%unix_time = ();
$users_dir = "dataset-$size/users";
%matches = ();

# get the cookies
%cookies = fetch CGI::Cookie;
$sid = $cookies{'sid'}->value if $cookies{'sid'};

$session = CGI::Session->load($sid);
$sid = $session->id();

 # if expired or empty return to respective places.
# empty - > bitter.cgi w/ new session
# expired - > logout..
if ($session->is_expired) {
    print $session->header(-location=>"login.cgi");
} elsif ($session->is_empty) {
    $session = $session->new() or die $session->errstr;      
    print $session->header(-location=>"search.cgi?q=$regex");
    
# if bleats was called display only bleats
} elsif (param('action') eq 'bleats') {
    print_bleats_search();
    
# else if only searching for users..
} elsif (param('action') eq 'users') {
    print_user_search()
} else {
    print_both();
}


# print both.... basic implementation
sub print_both {
    print page_header();
    print nav_bar();
    print search_options();
    print print_bleats();
    print print_users();
    print end_html;   
}


#print only bleat matches
sub print_bleats_search {
    print page_header();
    print nav_bar();
    print search_options();
    print print_bleats();
    print end_html;    

}


#find matching bleats..
sub bleats_search {

    # bleats directory
    $bleats_dir = "dataset-$size/bleats";
    
    # define the bleats..
    my @bleat_files = sort(glob("$bleats_dir/*"));
    my @matches;
    
    # open the bleat file and read the bleat content
    foreach $bleats (@bleat_files) {
        open my $p, "$bleats" or die "can not open $bleats: $!";
        my $bleat_details = join "<br>", <$p>;
        close $p;
        my @details = split /<br>/, $bleat_details;
        
        foreach my $val (@details) {
        
            # store content of bleats
            if ($val =~ /bleat:/) {
                $val =~ s/bleat: //;
                $bleat_content{$bleats} = $val;   
                
                # if the bleat contains the regex.. i.e. a word
                # or a hashtagged-string..
                if (($val =~ /$regex/gi) || ($val =~ /"$regex"/i && $regex =~ /^#/)) {
                    #push @matches, $bleats;
                    $matches{$bleats} = $bleats
                }
            
            #store the username for other nefarious reasons
            } elsif ($val =~ /username:/) {
                $val =~ s/username: //;
                chomp $val;
                $username{$bleats} = $val;
                
            # time of bleat    
            } elsif ($val =~ /time:/) {
                $val =~ s/time: //;
                $unix_time{$bleats} = $val;      
            }  
        }
    }
    #return @matches;
}

# print the bleats
sub print_bleats {
    
    bleats_search();    
    my $html = "<div class=\"container\"><h3><strong>Bleats</strong></h3><br>\n";

    # order the bleats in reverse chronological order
    my @ordered_bleats = ();
    foreach my $bleat (sort { $unix_time{$b} <=> $unix_time{$a} } keys %matches) {
        push @ordered_bleats, $bleat;
    }    


     
    if (!@ordered_bleats) {
        $html .= "<h4>Sorry no matches were found!</h4>\n";
    } else {    
        # for each bleat match print out bleats that match
        # in reverse chronological order
        my $col = 0;
        foreach my $match (@ordered_bleats) {

            # if image exists print the profile, else print the default img
            my $profile_pic = "$users_dir/$username{$match}/profile.jpg";
            my $img = img({class=>"img-circle center", src=>"http://www.clker.com/cliparts/Y/T/z/A/0/r/happy-lemon-hi.png"});
            $img = img({class=>"img-rounded center", src=>$profile_pic}) if -f $profile_pic;
        
            # get the full name of the user
            $fullname = get_full_name("$users_dir/$username{$match}");
            
            # print gridtype 'row' every 3 users
            if ($col % 3 == 0) {
                $html .= "<div class=\"row\">\n";
            }
            
            # get the date of post
            my $dt = strftime ("%d-%m-%Y %H:%M:%S", localtime ($unix_time{$match}));
            
            # print out user outputs
            $html .= <<eof
                            <div class="col-md-4">
                                <div class="col-md-5">
                                    <a href="bitter.cgi?action=profile&amp;username=$username{$match}">
                                        $img
                                    </a>
                                </div>
                                <div class="col-md-7">
                                    <center><p>
                                            $dt
                                        </p>
                                    </h4><h4><i>
                                        <a href="bitter.cgi?action=profile&amp;username=$username{$match}">
                                            \@$username{$match}
                                        </a>
                                    </i></h4><p>
                                        $bleat_content{$match}
                                    </p></center>
                                </div>    
                            </div><!--/.col-xs-6.col-lg-4-->
eof
                ;
                
            # increment columns    
            $col++;
            # if the column is divisible by 3 that means its end of row
            if ($col % 3 == 0) {
                $html .= "</div><br>\n";
            }

        }
        
        # makes sure the rows finish to keep div tags consistent
        if ($col % 3 != 0) {
            $html .= "</div>\n";
        }
    }
    
    if (param('action') eq 'bleats') {
    # finish with a footer
        $html .= <<eof
              <hr>
              <footer>
                <p>&copy; BitterSweet 2015</p>
              </footer>
              </div>

eof
            ;
    }
    return $html;
}


# PRINT ONLY USER MATCHES
sub print_user_search {
    print page_header();
    print nav_bar();
    print search_options();
    print print_users();
    print end_html;    
}


# find the user/name
sub user_search {

    # user directory
    $users_dir = "dataset-$size/users";
    
    # define the users..
    my @users = sort(glob("$users_dir/*"));
    my @matches;

    # find matches for directories
    foreach $user (@users) {
        $user =~ s/$users_dir\///;
        if ($user =~ /$regex/i) {
            push @matches, $user;
        } else {
            
            # otherwise see if their full name contains the regex..
            $name = get_full_name("$users_dir/$user");
            if ($name =~ /$regex/i) {
                push @matches, $user;
            }
        }
    }
    return @matches;
}


# print the users
sub print_users {
    
    my @matches = user_search();    
    my $html = "<h3><strong>Accounts</strong></h3><br><div class=\"container\">\n";
     
    if (!@matches) {
        $html .= "<h4>Sorry no matches were found!</h4>\n";
    } else {
        # for each user match print out users that match
        my $col = 0;
        foreach my $match (@matches) {
        
            # if image exists print the profile, else print the default img
            my $profile_pic = "$users_dir/$match/profile.jpg";
            my $img = img({class=>"img-circle center", src=>"http://www.clker.com/cliparts/Y/T/z/A/0/r/happy-lemon-hi.png"});
            $img = img({class=>"img-circle center", src=>$profile_pic}) if -f $profile_pic;
        
            # get the full name of the user
            $fullname = get_full_name("$users_dir/$match");
            # get the suburb of the user
            $suburb = get_suburb("$users_dir/$match");
            
            # print gridtype 'row' every 3 users
            if ($col % 3 == 0) {
                $html .= "<div class=\"row\">\n";
            }
            
            # print out user outputs
            $html .= <<eof
                            <div class="col-md-4">
                                <div class="col-md-5">
                                    <a href="bitter.cgi?action=profile&amp;username=$match">
                                        $img
                                    </a>
                                </div>
                                <div class="col-md-7">
                                    <center><h4>
                                        <a href="bitter.cgi?action=profile&amp;username=$match">
                                            $fullname
                                        </a>
                                    </h4><h4><i>
                                        <a href="bitter.cgi?action=profile&amp;username=$match">
                                            \@$match
                                        </a>
                                    </i></h4><p>
                                        Lives close by $suburb
                                    </p></center>
                                </div>    
                            </div><!--/.col-xs-6.col-lg-4-->
eof
                ;
                
            # increment columns    
            $col++;
            # if the column is divisible by 3 that means its end of row
            if ($col % 3 == 0) {
                $html .= "</div><br>\n";
            }

        }
        
        # makes sure the rows finish to keep div tags consistent
        if ($col % 3 != 0) {
            $html .= "</div>\n";
        }
    }
    
    
    # finish with a footer
    $html .= <<eof
          <hr>
          <footer>
            <p>&copy; BitterSweet 2015</p>
          </footer>
          </div>

eof
        ;
    return $html;
}



# prints nav bar + search query
sub nav_bar {
    return <<eof
        <nav class="navbar navbar-inverse navbar-fixed-top">
          <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Bitter</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Settings</a></li>
                <li><a href="bitter.cgi?action=profile">Profile</a></li>
                <li><a href="login.cgi?action=logout">Logout</a></li>
              </ul>
              <form class="navbar-form navbar-right" action="search.cgi">
                <input type="text" class="form-control" placeholder="Search..." name="q">
              </form>
            </div>  
          </div>
        </nav>
        <div class="cust-jumbo">
            <div class="container">
                <h1><center>Search Phrase: $regex</center></h1>
            </div>
        </div>
eof
        ;
}

# prints search options
sub search_options {
    # remember to add is-selected back to line 57
    return <<eof
        <div class="AppContainer">
            <div class="AdaptiveFiltersBar">
                <ul class="AdaptiveFiltersBar-nav">
                    <li class="AdaptiveFiltersBar-item u-borderUserColor">
                        <a class="AdaptiveFiltersBar-target AdaptiveFiltersBar-target--link u-textUserColor js-nav" href="search.cgi?q=$regex">
                            <span class="AdaptiveFiltersBar-label">All</span>
                        </a>
                    </li><li class="AdaptiveFiltersBar-item u-borderUserColor ">
                        <a class="AdaptiveFiltersBar-target AdaptiveFiltersBar-target--link u-textUserColor js-nav" href="search.cgi?action=users&amp;q=$regex">
                            <span class="AdaptiveFiltersBar-label">Accounts</span>
                        </a>
                    </li><li class="AdaptiveFiltersBar-item u-borderUserColor ">
                        <a class="AdaptiveFiltersBar-target AdaptiveFiltersBar-target--link u-textUserColor js-nav" href="search.cgi?action=bleats&amp;q=$regex">
                            <span class="AdaptiveFiltersBar-label">Bleats</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
eof
        ;
}

# prints header..
sub page_header {
    return header,
        start_html(-title => 'Bitter User Search', -style => [{src=>"css/bootstrap.min.css"},
                                                  {src=>"bitter.css"},
                                                  {src=>"css/bootstrap-theme.css"},
                                                  {src=>"css/boostrap.css"},
                                                  {src=>"search.css"},],);
}

# get full name
sub get_full_name {
    my $details_filename = "$_[0]/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $! ";
    my $details = join "<br>", <$p>;
    close $p;

    my @details = split /<br>/, $details;    

    foreach my $val (@details) {
        if ($val =~ /full_name:/) {
            $val =~ s/full_name: //;
            return $val;
        }            
    }
}

#get suburb
sub get_suburb {
my $details_filename = "$_[0]/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $! ";
    my $details = join "<br>", <$p>;
    close $p;

    my @details = split /<br>/, $details;    

    foreach my $val (@details) {
        if ($val =~ /home_suburb:/) {
            $val =~ s/home_suburb: //;
            return $val;
        }            
    }
}

