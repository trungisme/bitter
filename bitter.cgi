#!/usr/bin/perl -w

# written by andrewt@cse.unsw.edu.au September 2015
# as a starting point for COMP2041/9041 assignment 2
# http://cgi.cse.unsw.edu.au/~cs2041/assignments/bitter/

use CGI qw/:all/;
use CGI::Carp qw(fatalsToBrowser warningsToBrowser);
use POSIX qw( strftime );
use CGI::Cookie;
use CGI::Session;

#user details
%user_details = ();


sub main() {
	
	# define some global variables
	$debug = 1;
	$dataset_size = "medium"; 
	$users_dir = "dataset-$dataset_size/users";
	$bleats_dir = "dataset-$dataset_size/bleats";

    # get the cookies
    %cookies = fetch CGI::Cookie;
    $username = $cookies{'username'}->value if $cookies{'username'};
    $sid = $cookies{'sid'}->value if $cookies{'sid'};
    
    $session = CGI::Session->load($sid);
    $sid = $session->id();
    $cur_user_details = "$users_dir/$username";
    
    # parameter for bleats_list
    $bleats_file = "$cur_user_details/bleats.txt";
    
    # if expired or empty return to respective places.
    # empty - > bitter.cgi w/ new session
    # expired - > logout..
    if ($session->is_expired) {
        print $session->header(-location=>"login.cgi");
    } elsif ($session->is_empty) {
        $session = $session->new() or die $session->errstr;      
        print $session->header(-location=>"bitter.cgi"); 
    
    # if the user has sent a new bleat then CREATE IT..
    } elsif (param('bleat')) {
        new_bleats_handle();
        print_cur_user_page($cur_user_details);
        
    # if username exists i.e. produced from links 
    # print out THAT users profile page WITHOUT 'send' bleats 
    } elsif (param('username')) {
        my $alt_user = param('username');
        $alt_user = substr $alt_user, 0, 256;
        print_user_int_page("$users_dir/$alt_user");
    
    #otherwise, request is to print standard profile
    } else {
        print_cur_user_page($cur_user_details);
	}
}

# print the user of interest's page
sub print_user_int_page {
    print page_header(), "\n";
    # Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    print user_body($_[0]);
    print user_bleats($_[0]);
    print page_trailer();
}


#print the current user page via their functions
sub print_cur_user_page {
    print page_header(), "\n";
    # Now tell CGI::Carp to embed any warning in HTML
    warningsToBrowser(1);
    print user_body($_[0]);
    print send_bleats();
    print user_bleats($_[0]);
    print page_trailer();
}


# send bleats form
sub send_bleats {
    return <<eof
    <div class ="col-md-9">
        <div class="row">
        <blockquote class="twitter_tweet">
        <form method="post">
            <textarea type="text" id="styled" name='bleat' placeholder='Send a Bleat...' rows=5 cols=40 maxlength=142></textarea><br><br>
            <button type="submit" name="Bleat it!" class="btn btn-lg btn-danger aboutfont" style="float: right;"><center>Bleat it!</center></button>
        </form>
        </blockquote>
        </div>
eof
    ;

}



# save bleats and make it output-able
sub new_bleats_handle {
    
    # obtain the bleat from param passed through submit
    my $bleat = param('bleat');
    
    # obtain local time from browser/system (in epoch time)
    my $bleat_time = time;
    
    # obtain a random idgen
    # use first half of id as bleat_no
    my $bleat_no = `uuidgen`;
    my $bleat_name = substr $bleat_no, 0, int (length($bleat_no) / 2);
    
    # if file exists on a whim, use second half of string
    # pray that this works, not going to implement a better solution yet..
    if (-f "$bleats_dir/$bleat_name") {
        $bleat_name = substr $bleat_no, int(length ($bleat_no) / 2) + 1;
    }
    $bleat_name =~ s/\W+//g; # make the id a tiny bit neater..
    
    # create the bleat file.. unable to implement latitude and longitude yet :/ soon !
    open F, ">$bleats_dir/$bleat_name" or die "Unable to open $bleat_name: $!";
    print F "username: $username\n";
    print F "time: $bleat_time\n";
    #print file "latitude: $bleat_latitude\n";
    #print file "longitude: $bleat_longitude\n";
    print F "bleat: $bleat\n";
    close F;
    
    # obtain the content within the bleats.txt
    open my $F, "<$cur_user_details/bleats.txt" or die "Unable to open $cur_user_details/bleats.txt: $!";
    $bleats_list = join "", <$F>;
    close $F;
    $bleats_list .= "\n$bleat_name";
    
    # add back the content with the new bleat id into the text file for that user :)
    if (open FILE, ">$cur_user_details/bleats.txt") {
    	print FILE $bleats_list;
    	close FILE;
    }
    
}





#
# Show fancy bar at top
#
sub user_body {
    
    #get user information
    my $user_to_show = $_[0];
    get_user_info($user_to_show);
    
    
    #get profile picture for the user
    my $profile_pic = "$user_to_show/profile.jpg";
    my $img = img({src=>"http://www.clker.com/cliparts/Y/T/z/A/0/r/happy-lemon-hi.png", class=>"img-circle center"});
    $img = img({src=>$profile_pic, class=>"img-thumbnail center"}) if -f $profile_pic;
    
       
    #print final div tag
    $detail .= "    </ol></div>\n";
    # would put in html file.. but don't want to risk due to time factors
    my $body = <<eof
    
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Bitter</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Settings</a></li>
            <li><a href="bitter.cgi?action=profile">Profile</a></li>
            <li><a href="login.cgi?action=logout">Logout</a></li>
          </ul>
          <form class="navbar-form navbar-right" action="search.cgi">
            <input type="text" class="form-control" placeholder="Search..." name="q">
          </form>
        </div>  
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">$img
          <div class="about-font">
            <ul class="nav nav-sidebar">
                <li><br></li>
                <li><b><h3>$user_details{'full name'}</h3></b></li>
                <li><i>\@$user_details{'username'}</i></li><br>
                <li><h4>About Me</h4></li>
                <li>Suburb: $user_details{'suburb'}</li>
                <li>Longitude: $user_details{'longitude'}</li>
                <li>Latitude: $user_details{'latitude'}</li>
            </ul>
            <ul class="nav nav-sidebar">
                <li><h4>Following:</h4></li>
eof
    ;            
    # split followers into separate strings
    @following = split " ", $user_details{'listens'};
    foreach $follow (@following) {
        $body .= "                  <li><a href=\"bitter.cgi?action=profile&amp;username=$follow\"><i>\@$follow</i></a></li>\n";
    }
              
    $body .= "          </ul></div></div>\n";

=pop
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item</a></li>
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
            <li><a href="">More navigation</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="">Nav item again</a></li>
            <li><a href="">One more nav</a></li>
            <li><a href="">Another nav item</a></li>
          </ul>
=cut
    return $body;
}


# return the users bleats..
sub user_bleats {

    my $user_to_show  = $_[0];
    my $bleats_list = "$user_to_show/bleats.txt";
    
    #bleats details
    my %in_reply_to = ();  # bleat , given by key is a response to value.
    my %bleat_content = (); # contains content of bleat
    my %unix_time = ();
    my %longitude = ();
    my %latitude = ();
    
    
    
    open my $p, "$bleats_list" or die "can not open $bleats_list: $!";
    my @bleats = split / |\t|\n/, lc(do {local $/; <$p> }); 
    close $p;
    
    # this print checks if corresponding bleats file has been opened and read.
    #print div({-class=>"col-md-2"}, "@bleats\n"), "\n";
    
    
    
    for $bleats (@bleats) {
        open my $p, "$bleats_dir/$bleats" or die "can not open $bleats_dir/$bleats: $!";
        my $bleat_details = join "<br>", <$p>;
        close $p;
        my @details = split /<br>/, $bleat_details;
        
        foreach my $val (@details) {
            if ($val =~ /username:/) {
                $val =~ s/username: //;
                
                # makes sure that the username correspondes to the users page..
                if ($val ne $user_details{'username'}) { 
                    print "you fucked up somewhere \n";
                }
            
            # bleat , given by key is a response to value.    
            } elsif ($val =~ /in_reply_to:/) {
                $val =~ s/in_reply_to: //;
                $in_reply_to{$bleats} = $val;
                
            # store content of bleats
            } elsif ($val =~ /bleat:/) {
                $val =~ s/bleat: //;
                $bleat_content{$bleats} = $val;    
            
            # time of bleat    
            } elsif ($val =~ /time:/) {
                $val =~ s/time: //;
                $unix_time{$bleats} = $val;
            
            # store location
            } elsif ($val =~ /longitude:/) {
                $val =~ s/longitude: //;
                $longitude{$bleats} = $val;
            } elsif ($val =~ /latitude:/) {
                $val =~ s/latitude: //;
                $latitude{$bleats} = $val;
            }
        }

        
    }   
    
    # order the bleats in reverse chronological order
    my @ordered_bleats = ();
    foreach my $bleat_no (sort { $unix_time{$b} <=> $unix_time{$a} } keys %unix_time) {
        push @ordered_bleats, $bleat_no;
    }
    
       
    foreach my $bleat (@ordered_bleats) {
        # convert to readable date format.
        my $dt = strftime ("%d-%m-%Y %H:%M:%S", localtime ($unix_time{$bleat}));
        $body .= <<eof
        
<div class="row">
    <blockquote class="twitter_tweet">
        <h3><b>$user_details{'full name'}</b></h3>
        <h5><i>\@$user_details{'username'}</i></h5>
        <h5>$dt</h5>
        <h5>Longitude: $latitude{$bleat} Latitude: $longitude{$bleat}</h5>
        <h5>$bleat</h5>
        <p>$bleat_content{$bleat}</p>
    </blockquote>
</div>
eof
            ;
    }
    
    
    
    #$body .= "@ordered_bleats", "\n"; 
    
    $body .= <<eof
    </div>
</div>
    <footer class="footer">
        <p>&copy; BitterSweet 2015</p>
    </footer>
</div> <!-- /container -->
</div>
eof
    ;
    
    return $body;
}


#get user details from database
sub get_user_info {
    
    my $user_to_show = $_[0];
    my $details_filename = "$user_to_show/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $! ";
    my $details = join "<br>", <$p>;
    close $p;
    
    my @details = split /<br>/, $details;    
    
    # filter each detail and store it in the hash
    # as of now, unsure if security breach.. keep in mind for later.
    foreach my $val (@details) {
    
        if ($val =~ /email:/) {
            $val =~ s/email: //;
            $user_details{'email'} = $val;
        } elsif ($val =~ /full_name:/) {
            $val =~ s/full_name: //;
            $user_details{'full name'} = $val;        
        } elsif ($val =~ /username:/) {
            $val =~ s/username: //;
            $user_details{'username'} = $val;
        } elsif ($val =~ /home_suburb:/) {
            $val =~ s/home_suburb: //;
            $user_details{'suburb'} = $val;
        } elsif ($val =~ /home_longitude:/) {
            $val =~ s/home_longitude: //;
            $user_details{'longitude'} = $val;
        } elsif ($val =~ /home_latitude:/) {
            $val =~ s/home_latitude: //;
            $user_details{'latitude'} = $val;
        } elsif ($val =~ /listens:/) {
            $val =~ s/listens: //;
            $user_details{'listens'} = $val;
        }
    }
}


#
# HTML placed at the top of every page
#
sub page_header {
    return header,
        start_html(-title => 'Bitter', -style => [{src=>"css/bootstrap.min.css"},
                                                  {src=>"bitter.css"},
                                                  {src=>"css/bootstrap-theme.css"},
                                                  {src=>"css/boostrap.css"},
                                                  {src=>"css/bootstrap-theme.min.css"},],);
}


#
# HTML placed at the bottom of every page
# It includes all supplied parameter values as a HTML comment
# if global variable $debug is set
#
sub page_trailer {
    my $html = "";
    $html .= join("", map("<!-- $_=".param($_)." -->\n", param())) if $debug;
    $html .= "<!-- $sid --!>\n";
    $html .= "<!-- $username --!>\n";
    $html .= end_html;
    return $html;
}

main();
