#!/usr/bin/perl -w

use CGI qw/:all/;
use CGI::Carp qw/fatalsToBrowser warningsToBrowser/;

$username = param('username') || '';
$password = param('password') || '';
$username = substr $username, 0, 256;


$size = "small";
$users_dir = "dataset-$size/users";
$cur_user_dir = "$users_dir/$username";


print header, start_html(-title => 'Bitter', -style => [{src=>"css/bootstrap.min.css"},
                                                        {src=>"bitter.css"},],);
                                                        
warningsToBrowser(1);
if ($username && $password) {
    
    #if the directory exists print this..
    if (-d $cur_user_dir) {
    
    
         # compare with actual pw
        $actual_password = get_password($cur_user_dir);
        $actual_password =~ s/\s+//;
        
        # if password is true, cont. else print err msg
        if ($actual_password eq $password) {
            print "$username authenticated.\n";
        } else {
            print "Incorrect password!\n";
        }
    } else {
        print "Unknown Username!"
    }
           
} else {
    print standard_form();
    ;
}
print end_html;
exit(0);

sub get_password {
    my $user_to_show = $_[0];
    my $details_filename = "$user_to_show/details.txt";
    open my $p, "$details_filename" or die "can not open $details_filename: $! ";
    my $details = join "<br>", <$p>;
    close $p;
    
    my $password = '';
    my @details = split /<br>/, $details;    
    
    foreach my $val (@details) {
    
        if ($val =~ /password:/) {
            $val =~ s/password: //;
            return $password = $val;
        }
    }
}

sub standard_form {
    return <<eof
        <div class="container">
            <form class="form-signin" method="post">
                <h2 class="form-signin-heading">Please sign in</h2>
                <label for="inputUsername" class="sr-only">Username</label>
                <input type="text" id="inputUsername" class="form-control" placeholder="Username" name='username' required autofocus>
                <label for="inputPassword" class="sr-only">Password</label>
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" name='password' required>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label> 
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        </div> <!-- /container -->    
eof
    ;
}
